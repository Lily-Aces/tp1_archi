<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste Produits</title>
</head>
<body>
    
    <h1>Sélection des produits</h1>
    
    <c:set var="Panier" value="${sessionScope.Panier}" />
    
    <form action="UpdateListeProduitsServlet" name="Liste de Produits" method="post">
        
        <label for="nbStylo">Stylo</label>
        <input type="number" name="nbStylo" id="nbStylo" value="${Panier.nbStylo}" required/>
        
        <label for="nbFeutre">Feutre</label>
        <input type="number" name="nbFeutre" id="nbFeutre" value="${Panier.nbFeutre}" required/>
        
        <label for="nbGomme">Gomme</label>
        <input type="number" name="nbGomme" id="nbGomme" value="${Panier.nbGomme}" required/>
        
        <input type="submit" value="Valider"/>
    
    </form>
    
    <a href="AfficheProjetServlet">Afficher le panier</a>

</body>
</html>