<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Panier</title>
</head>
<body>
    
    <h1>Contenu du panier</h1>
    
    <c:set var="Panier" value="${sessionScope.Panier}" />
    
    <div>
        
        <h2>Votre panier</h2>
        
        <p>Stylo <c:out value="${Panier.nbStylo}" /></p>
        <p>Feutre <c:out value="${Panier.nbFeutre}" /></p>
        <p>Gomme <c:out value="${Panier.nbGomme}" /></p>
    
    </div>
    
    <div>
        
        <h2>Récapitulatif</h2>
        
        <p>Panier : <c:out value="${Panier.prixPanier}" /></p>
        <p>Frais de livraison : <c:out value="${Panier.prixLivraison}" /></p>
        <p>Prix total : <c:out value="${Panier.prixTotal}" /></p>
    
    </div>
    
    <a href="listeProduits.jsp">Retourner choisir des produits</a>

</body>
</html>