package edu.polytech.archi.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.polytech.archi.bean.PanierBean;
import edu.polytech.archi.metier.PanierMetier;

/**
 * Servlet implementation class UpdateListeProduitsServlet
 */
public class UpdateListeProduitsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateListeProduitsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		int nbStylo = Integer.parseInt(request.getParameter("nbStylo"));
		int nbFeutre = Integer.parseInt(request.getParameter("nbFeutre"));
		int nbGomme = Integer.parseInt(request.getParameter("nbGomme"));
		
		HttpSession session = request.getSession();
		
		PanierBean panier;
		
		if(session.getAttribute("Panier") == null) {
			panier = new PanierBean();
		} else {
			panier = (PanierBean) session.getAttribute("Panier");
		}
		
		panier.setNbFeutre(nbFeutre);
		panier.setNbGomme(nbGomme);
		panier.setNbStylo(nbStylo);
				
		PanierMetier.effectuerCalculsPanier(panier);
		
		session.setAttribute("Panier", panier);
		request.getRequestDispatcher("/listeProduits.jsp").forward( request, response );

	}

}
