package edu.polytech.archi.bean;

public class PanierBean {
	
	private static final double PRIXSTYLO = 1.20;
	private static final double PRIXFEUTRE = 1.80;
	private static final int PRIXGOMME = 2;
	
	private int nbStylo;
	private int nbFeutre;
	private int nbGomme;
	private double prixPanier;
	private int prixLivraison;
	private double prixTotal;
	
	public PanierBean() {
		nbStylo = 0;
		nbFeutre = 0;
		nbGomme = 0;
		prixPanier = 0;
		prixLivraison = 0;
		prixTotal = 0;
	}

	public int getNbStylo() {
		return nbStylo;
	}

	public void setNbStylo(int nbStylo) {
		this.nbStylo = nbStylo;
	}

	public int getNbFeutre() {
		return nbFeutre;
	}

	public void setNbFeutre(int nbFeutre) {
		this.nbFeutre = nbFeutre;
	}

	public int getNbGomme() {
		return nbGomme;
	}

	public void setNbGomme(int nbGomme) {
		this.nbGomme = nbGomme;
	}

	public double getPrixPanier() {
		return prixPanier;
	}
	
	public void setPrixPanier(double prixPanier) {
		this.prixPanier = prixPanier;
	}


	public int getPrixLivraison() {
		return prixLivraison;
	}

	public void setPrixLivraison(int prixLivraison) {
		this.prixLivraison = prixLivraison;
	}

	public double getPrixTotal() {
		return prixTotal;
	}
	
	public void setPrixTotal(double prixTotal) {
		this.prixTotal = prixTotal;
	}


	
	

}
