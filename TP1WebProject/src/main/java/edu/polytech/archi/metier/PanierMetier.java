package edu.polytech.archi.metier;

import edu.polytech.archi.bean.PanierBean;

public class PanierMetier {
	
	private static final double PRIXSTYLO = 1.20;
	private static final double PRIXFEUTRE = 1.80;
	private static final int PRIXGOMME = 2;
	
	public static void effectuerCalculsPanier(PanierBean Panier) {
		Panier.setPrixPanier(Panier.getNbFeutre()*PRIXFEUTRE + Panier.getNbGomme()*PRIXGOMME + Panier.getNbStylo()*PRIXSTYLO);
		if(Panier.getPrixPanier() < 10) {
			Panier.setPrixLivraison(10);
		}
		else {
			Panier.setPrixLivraison(0);
		}
		Panier.setPrixTotal(Panier.getPrixPanier() + Panier.getPrixLivraison());
	}

}
